package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javax.swing.*;
import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;

public class calculationController implements Initializable {
    private Stage stage;
    private Scene scene;
    private Parent root;
    @FXML
    private TextField prviStup;
    @FXML
    private TextField drugiStup;
    @FXML
    private TextField raspon;
    @FXML
    private TextField promjer;
    @FXML
    private TextField presjek;
    @FXML
    private TextField masa;
    @FXML
    private TextField modul;
    @FXML
    private TextField koeficijent;
    @FXML
    private TextField normalnoIstezanje;
    @FXML
    private TextField iznimnoIstezanje;
    @FXML
    private TextField maksimalnoIstezanje;

    public static  TextField static_promjer;
    public static  TextField static_presjek;
    public static  TextField static_masa;
    public static  TextField static_modul;
    public static  TextField static_koeficijent;
    public static  TextField static_normlano;
    public static  TextField static_iznimno;
    public static  TextField static_maksimalno;
    public static  TextField static_h1;
    public static  TextField static_h2;
    public static  TextField static_raspon;


    private double visinaPrvogStupa = 0;
    private double visinaDrugogStupa = 0;
    private double rasponStupova;
    private double promjerVodica;
    private double presjekVodica;
    private double uzduznaMasa;
    private double modulElasticnosti;
    private double koeficijentIstezanja;
    private double normalnoDozvoljeno;
    private double iznimnoDozvoljeno;
    private double maksimalnoDozvoljeno;
    private double denivelacija;
    private double spojnica;
    private double vlastitaTezina;
    private double gravitacija = 9.81;
    private double reduciranaTezina;
    private double dodatnoOpterecenje;
    private double reduciranaTezinaZaledenogVodica;
    private double kriticniRaspon;
    private double pocetnaTemperatura = 0;
    private double pocetnaTezina = 0;
    private double pocetnoIstezanje = 0;
    private double nadomjesnoNaprezanje;
    private double stvarnaTezina;
    private double temperatura5 = -5;
    private double temperatura20 = -20;
    private double temperatura40 = 40;
    private double naprezanje5;
    private double naprezanje20;
    private double naprezanje40;
    private double provjes5;
    private double provjes20;
    private double provjes40;
    private Integer id;
    public void povezivanjeVarijabli(){
        visinaPrvogStupa = Double.parseDouble(prviStup.getText());
        visinaDrugogStupa = Double.parseDouble(drugiStup.getText());
        rasponStupova = Double.parseDouble(raspon.getText());
        promjerVodica = Double.parseDouble(promjer.getText());
        presjekVodica = Double.parseDouble(presjek.getText());
        uzduznaMasa = Double.parseDouble(masa.getText());
        modulElasticnosti = Double.parseDouble(modul.getText());
        koeficijentIstezanja = Double.parseDouble(koeficijent.getText());
        normalnoDozvoljeno = Double.parseDouble(normalnoIstezanje.getText());
        iznimnoDozvoljeno = Double.parseDouble(iznimnoIstezanje.getText());
        maksimalnoDozvoljeno = Double.parseDouble(maksimalnoIstezanje.getText());
        checkIfZero();
        ispisRezultata();
    }
    public void checkIfZero(){
        if(visinaPrvogStupa==0){
            JOptionPane.showMessageDialog(null,"Unesite visinu prvog stupa");
        }
        if(visinaDrugogStupa==0){
            JOptionPane.showMessageDialog(null,"Unesite visinu drugog stupa");
        }
        if(rasponStupova==0){
            JOptionPane.showMessageDialog(null,"Unesite raspon");
        }
        if(promjerVodica==0){
            JOptionPane.showMessageDialog(null,"Unesite promjer vodiča");
        }
        if(presjekVodica==0){
            JOptionPane.showMessageDialog(null,"Unesite presjek vodiča");
        }
        if(uzduznaMasa==0){
            JOptionPane.showMessageDialog(null,"Unesite uzdužnu masu vodiča");
        }
        if(modulElasticnosti==0){
            JOptionPane.showMessageDialog(null,"Unesite modul elastičnosti");
        }
        if(koeficijentIstezanja==0){
            JOptionPane.showMessageDialog(null,"Unesite linearni koeficijent istezanja");
        }
        if(normalnoDozvoljeno==0){
            JOptionPane.showMessageDialog(null,"Unesite normalno dozvoljeno istezanje");
        }
        if(iznimnoDozvoljeno==0){
            JOptionPane.showMessageDialog(null,"Unesite iznimno dozvoljeno istezanje");
        }
        if(maksimalnoDozvoljeno==0){
            JOptionPane.showMessageDialog(null,"Unesite maksimalno dozvoljeno istezanje");
        }
    }

    public void switchToScene1(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    public void switchToScene3(ActionEvent event) throws IOException {
        povezivanjeVarijabli();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("ispisRezultata.fxml"));
        root = loader.load();
        ispisRezultataController ispisRezultataController = loader.getController();
        ispisRezultataController.setProvjes5(Double.toString(provjes5));
        ispisRezultataController.setProvjes20(Double.toString(provjes20));
        ispisRezultataController.setProvjes40(Double.toString(provjes40));
        Connection connection = mySQLConnect.ConnectDB();
        try {
            String sql = "select * from Rezultat";
            Statement preparedStatement = connection.createStatement();
            ResultSet resultSet = preparedStatement.executeQuery(sql);
            while (resultSet.next()){
                id = resultSet.getInt("ID");
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        rezultat result = new rezultat(id,Double.toString(promjerVodica),Double.toString(presjekVodica),Double.toString(uzduznaMasa),
                Double.toString(modulElasticnosti),Double.toString(koeficijentIstezanja),Double.toString(normalnoDozvoljeno),Double.toString(iznimnoDozvoljeno),
                Double.toString(maksimalnoDozvoljeno),Double.toString(visinaPrvogStupa),Double.toString(visinaDrugogStupa),Double.toString(rasponStupova),
                Double.toString(provjes20),Double.toString(provjes5),Double.toString(provjes40));
        ispisRezultataController.CatchData(result);
        Stage stage1 = new Stage();

        stage1.setTitle("Kalkulator provjesa ");
        Image icon = new Image("sample/icon.jpg");
        stage1.getIcons().add(icon);
        stage1.setScene(new Scene(root));
        stage1.show();
    }
    public void switchToScene4(ActionEvent event) throws IOException {
        root = FXMLLoader.load(getClass().getResource("table.fxml"));
        Stage stage1 = new Stage();
        stage1.setTitle("Kalkulator provjesa ");
        Image icon = new Image("sample/icon.jpg");
        stage1.getIcons().add(icon);
        stage1.setScene(new Scene(root));
        stage1.show();
    }
    public void switchToScene5(ActionEvent event) throws IOException{
        root = FXMLLoader.load(getClass().getResource("history.fxml"));
        Stage stage1 = new Stage();
        stage1.setTitle("Kalkulator provjesa ");
        Image icon = new Image("sample/icon.jpg");
        stage1.getIcons().add(icon);
        stage1.setScene(new Scene(root));
        stage1.show();
    }
    public double izracunDenivelacije(){
        if(visinaDrugogStupa >visinaPrvogStupa){
            denivelacija = visinaDrugogStupa - visinaPrvogStupa;
        }else if(visinaPrvogStupa > visinaDrugogStupa){
            denivelacija = visinaPrvogStupa - visinaDrugogStupa;
        }else {
            denivelacija = 0;
        }
        return denivelacija;
    }
    public double izracunSpojnice(){
        return spojnica = Math.sqrt(Math.pow(izracunDenivelacije(),2)+Math.pow(rasponStupova,2));
    }
    public double izracunVlastiteTezine(){
        return vlastitaTezina = uzduznaMasa * gravitacija;
    }
    public double izracunReduciraneTezine(){ return reduciranaTezina = izracunVlastiteTezine()/presjekVodica; }
    public double izracunDodatnogOpterecenja(){
        return dodatnoOpterecenje = 0.18 * Math.sqrt(promjerVodica);
    }
    public double izracunReduciraneTezineZaledenogVodica(){
        return reduciranaTezinaZaledenogVodica = (izracunVlastiteTezine()+izracunDodatnogOpterecenja())/presjekVodica;
    }
    public double izracunKriticnogRaspona(){
        return kriticniRaspon = maksimalnoDozvoljeno * Math.sqrt((360*koeficijentIstezanja)/
                (Math.pow(izracunReduciraneTezineZaledenogVodica(),2)-Math.pow(izracunReduciraneTezine(),2)));
    }


    public void postavljanjePocetnogStanje(){
        if(rasponStupova>izracunKriticnogRaspona()){
            pocetnaTemperatura = -5;
            pocetnaTezina = izracunReduciraneTezineZaledenogVodica();
        }else {
            pocetnaTemperatura = -20;
            pocetnaTezina = izracunReduciraneTezine();
        }
        pocetnoIstezanje = maksimalnoDozvoljeno;
    }
    public double izracunNadomjesnogNaprezanja(){
        postavljanjePocetnogStanje();
        return nadomjesnoNaprezanje = pocetnoIstezanje * ((Math.pow(izracunSpojnice(),3)/
                Math.pow(rasponStupova,2))/(Math.pow(izracunSpojnice(),2)/rasponStupova));
    }

    public double cubicEquation(double temperatura){
        if(temperatura == -5){
            stvarnaTezina = izracunReduciraneTezineZaledenogVodica();
        }else {
            stvarnaTezina = izracunReduciraneTezine();
        }
        double a = -24;
        double b = (24*izracunNadomjesnogNaprezanja() + 24 * modulElasticnosti *
                koeficijentIstezanja * (-20 - temperatura) - ((modulElasticnosti*Math.pow(rasponStupova,2)
                *Math.pow(pocetnaTezina,2))/Math.pow(izracunNadomjesnogNaprezanja(),2)));
        double c = 0;
        double d = (modulElasticnosti*Math.pow(rasponStupova,2)*Math.pow(stvarnaTezina,2));
        double p = -b / (3 * a);
        double q = Math.pow(p,3) + (b*c - 3*a*d)/(6*Math.pow(a,2));
        double r = c/(3*a);
        double zagrada = Math.sqrt((Math.pow(q, 2) + Math.pow((r - Math.pow(p, 2)), 3)));
        double x = Math.pow((q+ zagrada),0.33333333) + Math.pow((q- zagrada),0.3333333) + p;
        return x;
    }

    public double izracunStvarnogNaprezanja(double naprezanje){
        return naprezanje * ((Math.pow(izracunSpojnice(),2)/rasponStupova)/
                (Math.pow(izracunSpojnice(),3)/Math.pow(rasponStupova,2)));
    }
    public double izracunProvjesa(double stvarnoNaprezanje){
        return (Math.pow(rasponStupova,2)*stvarnaTezina*izracunSpojnice())/(8*stvarnoNaprezanje*rasponStupova);
    }


    public void ispisRezultata(){
        naprezanje5 = cubicEquation(temperatura5);
        double stvarnoNaprezanje5 = izracunStvarnogNaprezanja(naprezanje5);
        provjes5 = izracunProvjesa(stvarnoNaprezanje5);

        naprezanje20 = cubicEquation(temperatura20);
        naprezanje40 = cubicEquation(temperatura40);
        double stvarnoNaprezanje20 = izracunStvarnogNaprezanja(naprezanje20);
        double stvarnoNaprezanje40 = izracunStvarnogNaprezanja(naprezanje40);
        provjes20 = izracunProvjesa(stvarnoNaprezanje20);
        provjes40 = izracunProvjesa(stvarnoNaprezanje40);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        static_promjer = promjer;
        static_presjek = presjek;
        static_masa = masa;
        static_modul = modul;
        static_koeficijent = koeficijent;
        static_normlano = normalnoIstezanje;
        static_iznimno = iznimnoIstezanje;
        static_maksimalno = maksimalnoIstezanje;
        static_h1 = prviStup;
        static_h2 = drugiStup;
        static_raspon = raspon;
    }
}
