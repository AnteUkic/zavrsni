<Query Kind="SQL">
  <Connection>
    <ID>2eef3662-c6e8-46e9-a84d-ae2f97f96aea</ID>
    <Persist>true</Persist>
    <Server>DESKTOP-7Q92I23\SQLEXPRESS</Server>
    <Database>Vodici</Database>
    <ShowServer>true</ShowServer>
  </Connection>
</Query>

CREATE TABLE Vodic (
	ID int PRIMARY KEY,
	PromjerVodica DECIMAL(15,10),
	PresjekVodica DECIMAL(15,10),
	UzduznaMasa DECIMAL(15,10),
	ModulElasticnosti DECIMAL(15,10),
	KoeficijentIstezanja DECIMAL(15,10),
	NormalnoIstezanje DECIMAL(15,10),
	IznimnoIstezanje DECIMAL(15,10),
	MaksimalnoIstezanje DECIMAL(15,10)
);

Insert into Vodic Values(1,6.8,27.8,0.097,81000,0.0000191,110,210,90);

SELECT * FROM Vodic


CREATE TABLE Rezultat (
	ID int PRIMARY KEY,
	PromjerVodica DECIMAL(15,10),
	PresjekVodica DECIMAL(15,10),
	UzduznaMasa DECIMAL(15,10),
	ModulElasticnosti DECIMAL(15,10),
	KoeficijentIstezanja DECIMAL(15,10),
	NormalnoIstezanje DECIMAL(15,10),
	IznimnoIstezanje DECIMAL(15,10),
	MaksimalnoIstezanje DECIMAL(15,10),
	h1 DECIMAL(15,10),
	h2 DECIMAL(15,10),
	raspon DECIMAL(15,10),
	temp20 DECIMAL(15,10),
	temp5 DECIMAL(15,10),
	temp40 DECIMAL(15,10)
);

SELECT * FROM Rezultat

Insert into Rezultat Values(1,6.8,27.8,0.097,81000,0.0000191,110,210,90,120,135,130,0,0,0);
