package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


import javax.swing.*;
import java.sql.*;

public class mySQLConnect {
    Connection connection = null;

    public static Connection ConnectDB(){
        try{
            Class.forName("org.sqlite.JDBC");
            Connection connection = DriverManager.getConnection("jdbc:sqlite:Vodici.db");
            return connection;
        }catch (Exception e){
            JOptionPane.showMessageDialog(null,e);
            return null;
        }
    }
    public static ObservableList<vodic> getData(){
        Connection connection = ConnectDB();
        ObservableList<vodic> list = FXCollections.observableArrayList();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from Vodic");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                list.add(new vodic(Integer.parseInt(resultSet.getString("ID")), (resultSet.getString("PromjerVodica")),
                        (resultSet.getString("PresjekVodica")), (resultSet.getString("UzduznaMasa")), (resultSet.getString("ModulElasticnosti")),
                        (resultSet.getString("KoeficijentIstezanja")), (resultSet.getString("NormalnoIstezanje")),
                        (resultSet.getString("IznimnoIstezanje")), (resultSet.getString("MaksimalnoIstezanje"))));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return list;
    }
    public static ObservableList<rezultat> getDataResult(){
        Connection connection = ConnectDB();
        ObservableList<rezultat> list = FXCollections.observableArrayList();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from Rezultat");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                list.add(new rezultat(Integer.parseInt(resultSet.getString("ID")), (resultSet.getString("PromjerVodica")),
                        (resultSet.getString("PresjekVodica")), (resultSet.getString("UzduznaMasa")), (resultSet.getString("ModulElasticnosti")),
                        (resultSet.getString("KoeficijentIstezanja")), (resultSet.getString("NormalnoIstezanje")),
                        (resultSet.getString("IznimnoIstezanje")), (resultSet.getString("MaksimalnoIstezanje")),
                        (resultSet.getString("h1")), (resultSet.getString("h2")),
                        (resultSet.getString("raspon")), (resultSet.getString("temp20")),
                        (resultSet.getString("temp5")), (resultSet.getString("temp40"))));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return list;
    }
}
