package sample;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class historyController extends calculationController implements Initializable {


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        UpdateTable();
    }

    @FXML
    private TableView<rezultat> table_users;

    @FXML
    private TableColumn<rezultat, Integer> col_ID;

    @FXML
    private TableColumn<rezultat, String> col_promjer;

    @FXML
    private TableColumn<rezultat, String> col_presjek;

    @FXML
    private TableColumn<rezultat, String> col_masa;

    @FXML
    private TableColumn<rezultat, String> col_modul;

    @FXML
    private TableColumn<rezultat, String> col_koeficijent;

    @FXML
    private TableColumn<rezultat, String> col_normalno;

    @FXML
    private TableColumn<rezultat, String> col_iznimno;

    @FXML
    private TableColumn<rezultat, String> col_maksimalno;

    @FXML
    private TableColumn<rezultat, String> col_h1;

    @FXML
    private TableColumn<rezultat, String> col_h2;

    @FXML
    private TableColumn<rezultat, String> col_raspon;

    @FXML
    private TableColumn<rezultat, String> col_20;

    @FXML
    private TableColumn<rezultat, String> col_5;

    @FXML
    private TableColumn<rezultat, String> col_40;

    public String value1;
    public String value2;
    public String value3;
    public String value4;
    public String value5;
    public String value6;
    public String value7;
    public String value8;
    public String value9;
    public String value10;
    public String value11;

    ObservableList<rezultat> list;

    int index = -1;

    Connection connection = null;
    ResultSet resultSet = null;
    PreparedStatement preparedStatement = null;

    @FXML
    void Uvezi(ActionEvent event) throws IOException {
        static_promjer.setText(value1);
        static_presjek.setText(value2);
        static_masa.setText(value3);
        static_modul.setText(value4);
        static_koeficijent.setText(value5);
        static_normlano.setText(value6);
        static_iznimno.setText(value7);
        static_maksimalno.setText(value8);
        static_h1.setText(value9);
        static_h2.setText(value10);
        static_raspon.setText(value11);

        ((Node)(event.getSource())).getScene().getWindow().hide();
    }

    @FXML
    void getSelected(MouseEvent event) {
        index = table_users.getSelectionModel().getSelectedIndex();
        if(index <= -1){
            return;
        }

        value1 = col_promjer.getCellData(index);
        value2 = col_presjek.getCellData(index);
        value3 = col_masa.getCellData(index);
        value4 = col_modul.getCellData(index);
        value5 = col_koeficijent.getCellData(index);
        value6 = col_normalno.getCellData(index);
        value7 = col_iznimno.getCellData(index);
        value8 = col_maksimalno.getCellData(index);
        value9 = col_h1.getCellData(index);
        value10 = col_h2.getCellData(index);
        value11 = col_raspon.getCellData(index);

        System.out.println(value1);
    }

    @FXML
    void obrisi_vodic(ActionEvent event) {
        connection = mySQLConnect.ConnectDB();
        String sql = "delete from Rezultat where ID = ?";
        index = table_users.getSelectionModel().getSelectedIndex();
        if(index <= -1){
            return;
        }
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,col_ID.getCellData(index).toString());
            preparedStatement.execute();
            JOptionPane.showMessageDialog(null, "Delete");
            UpdateTable();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    public void UpdateTable(){
        col_ID.setCellValueFactory(new PropertyValueFactory<rezultat,Integer>("ID"));
        col_promjer.setCellValueFactory(new PropertyValueFactory<rezultat,String>("promjer"));
        col_presjek.setCellValueFactory(new PropertyValueFactory<rezultat,String>("presjek"));
        col_masa.setCellValueFactory(new PropertyValueFactory<rezultat,String>("masa"));
        col_modul.setCellValueFactory(new PropertyValueFactory<rezultat,String>("elasticnost"));
        col_koeficijent.setCellValueFactory(new PropertyValueFactory<rezultat,String>("koeficijentIstezanja"));
        col_normalno.setCellValueFactory(new PropertyValueFactory<rezultat,String>("normlanoIstezanje"));
        col_iznimno.setCellValueFactory(new PropertyValueFactory<rezultat,String>("iznimnoIstezanje"));
        col_maksimalno.setCellValueFactory(new PropertyValueFactory<rezultat,String>("maksimalnoIstezanje"));
        col_h1.setCellValueFactory(new PropertyValueFactory<rezultat,String>("h1"));
        col_h2.setCellValueFactory(new PropertyValueFactory<rezultat,String>("h2"));
        col_raspon.setCellValueFactory(new PropertyValueFactory<rezultat,String>("raspon"));
        col_20.setCellValueFactory(new PropertyValueFactory<rezultat,String>("temp20"));
        col_5.setCellValueFactory(new PropertyValueFactory<rezultat,String>("temp5"));
        col_40.setCellValueFactory(new PropertyValueFactory<rezultat,String>("temp40"));

        list = mySQLConnect.getDataResult();
        table_users.setItems(list);
    }

}


