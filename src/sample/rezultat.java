package sample;

public class rezultat {
    Integer ID;
    String promjer;
    String presjek;
    String masa;
    String elasticnost;
    String koeficijentIstezanja;
    String normlanoIstezanje;
    String iznimnoIstezanje;
    String maksimalnoIstezanje;
    String h1;
    String h2;
    String raspon;
    String temp20;
    String temp5;
    String temp40;

    public Integer getID() {
        return ID;
    }

    public String getPromjer() {
        return promjer;
    }

    public String getPresjek() {
        return presjek;
    }

    public String getMasa() {
        return masa;
    }

    public String getElasticnost() {
        return elasticnost;
    }

    public String getKoeficijentIstezanja() {
        return koeficijentIstezanja;
    }

    public String getNormlanoIstezanje() {
        return normlanoIstezanje;
    }

    public String getIznimnoIstezanje() {
        return iznimnoIstezanje;
    }

    public String getMaksimalnoIstezanje() {
        return maksimalnoIstezanje;
    }

    public String getH1() {
        return h1;
    }

    public String getH2() {
        return h2;
    }

    public String getRaspon() {
        return raspon;
    }

    public String getTemp20() {
        return temp20;
    }

    public String getTemp5() {
        return temp5;
    }

    public String getTemp40() {
        return temp40;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public void setPromjer(String promjer) {
        this.promjer = promjer;
    }

    public void setPresjek(String presjek) {
        this.presjek = presjek;
    }

    public void setMasa(String masa) {
        this.masa = masa;
    }

    public void setElasticnost(String elasticnost) {
        this.elasticnost = elasticnost;
    }

    public void setKoeficijentIstezanja(String koeficijentIstezanja) {
        this.koeficijentIstezanja = koeficijentIstezanja;
    }

    public void setNormlanoIstezanje(String normlanoIstezanje) {
        this.normlanoIstezanje = normlanoIstezanje;
    }

    public void setIznimnoIstezanje(String iznimnoIstezanje) {
        this.iznimnoIstezanje = iznimnoIstezanje;
    }

    public void setMaksimalnoIstezanje(String maksimalnoIstezanje) {
        this.maksimalnoIstezanje = maksimalnoIstezanje;
    }

    public void setH1(String h1) {
        this.h1 = h1;
    }

    public void setH2(String h2) {
        this.h2 = h2;
    }

    public void setRaspon(String raspon) {
        this.raspon = raspon;
    }

    public void setTemp20(String temp20) {
        this.temp20 = temp20;
    }

    public void setTemp5(String temp5) {
        this.temp5 = temp5;
    }

    public void setTemp40(String temp40) {
        this.temp40 = temp40;
    }

    public rezultat(Integer ID, String promjer, String presjek, String masa, String elasticnost, String koeficijentIstezanja,
                    String normlanoIstezanje, String iznimnoIstezanje, String maksimalnoIstezanje, String h1, String h2, String raspon, String temp20, String temp5, String temp40) {
        this.ID = ID;
        this.promjer = promjer;
        this.presjek = presjek;
        this.masa = masa;
        this.elasticnost = elasticnost;
        this.koeficijentIstezanja = koeficijentIstezanja;
        this.normlanoIstezanje = normlanoIstezanje;
        this.iznimnoIstezanje = iznimnoIstezanje;
        this.maksimalnoIstezanje = maksimalnoIstezanje;
        this.h1 = h1;
        this.h2 = h2;
        this.raspon = raspon;
        this.temp20 = temp20;
        this.temp5 = temp5;
        this.temp40 = temp40;
    }


}
