package sample;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;


import javax.swing.*;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class tableController extends calculationController implements Initializable {

    @FXML
    private TextField txt_ID;

    @FXML
    private TextField txt_presjek;

    @FXML
    private TextField txt_masa;

    @FXML
    private TextField txt_modul;

    @FXML
    private TextField txt_koef;

    @FXML
    private TextField txt_promjer;

    @FXML
    private TextField txt_normalno;

    @FXML
    private TextField txt_iznimno;

    @FXML
    private TextField txt_maksimalno;

    @FXML
    private TableView<vodic> table_users;

    @FXML
    private TableColumn<vodic, Integer> col_ID;

    @FXML
    private TableColumn<vodic, String> col_promjer;

    @FXML
    private TableColumn<vodic, String> col_presjek;

    @FXML
    private TableColumn<vodic, String> col_masa;

    @FXML
    private TableColumn<vodic, String> col_modul;

    @FXML
    private TableColumn<vodic, String> col_koeficijent;

    @FXML
    private TableColumn<vodic, String> col_normalno;

    @FXML
    private TableColumn<vodic, String> col_iznimno;

    @FXML
    private TableColumn<vodic, String> col_maksimalno;

    ObservableList<vodic> list;

    int index = -1;

    Connection connection = null;
    ResultSet resultSet = null;
    PreparedStatement preparedStatement = null;

    @FXML
    void dodaj_vodic(ActionEvent event) {
        connection = mySQLConnect.ConnectDB();
        String sql = "insert into Vodic values(?,?,?,?,?,?,?,?,? )";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, Integer.parseInt(txt_ID.getText()));
            preparedStatement.setString(2, txt_promjer.getText());
            preparedStatement.setString(3, txt_presjek.getText());
            preparedStatement.setString(4, txt_masa.getText());
            preparedStatement.setString(5, txt_modul.getText());
            preparedStatement.setString(6, txt_koef.getText());
            preparedStatement.setString(7, txt_normalno.getText());
            preparedStatement.setString(8, txt_iznimno.getText());
            preparedStatement.setString(9, txt_maksimalno.getText());
            preparedStatement.execute();

            JOptionPane.showMessageDialog(null, "Vodic dodan uspjesno");
            UpdateTable();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    @FXML
    void getSelected(MouseEvent event) {
        index = table_users.getSelectionModel().getSelectedIndex();
        if(index <= -1){
            return;
        }
        txt_ID.setText(col_ID.getCellData(index).toString());
        txt_promjer.setText(col_promjer.getCellData(index));
        txt_presjek.setText(col_presjek.getCellData(index));
        txt_masa.setText(col_masa.getCellData(index));
        txt_modul.setText(col_modul.getCellData(index));
        txt_koef.setText(col_koeficijent.getCellData(index));
        txt_normalno.setText(col_normalno.getCellData(index));
        txt_iznimno.setText(col_iznimno.getCellData(index));
        txt_maksimalno.setText(col_maksimalno.getCellData(index));
    }

    @FXML
    void Edit(ActionEvent event) {
        try {
            connection = mySQLConnect.ConnectDB();
            String value1 = txt_ID.getText();
            String value2 = txt_promjer.getText();
            String value3 = txt_presjek.getText();
            String value4 = txt_masa.getText();
            String value5 = txt_modul.getText();
            String value6 = txt_koef.getText();
            String value7 = txt_normalno.getText();
            String value8 = txt_iznimno.getText();
            String value9 = txt_maksimalno.getText();

            String sql = "update Vodic set ID= '"+value1+"',PromjerVodica= '"+value2+"',PresjekVodica= '"+
                    value3+"',UzduznaMasa= '"+value4+"',ModulElasticnosti= '"+value5+"',KoeficijentIstezanja= '"+value6+"',NormalnoIstezanje= '"+
                    value7+"',IznimnoIstezanje= '"+value8+"',MaksimalnoIstezanje= '"+value9+"' where ID='"+value1+"' ";
            preparedStatement= connection.prepareStatement(sql);
            preparedStatement.execute();
            JOptionPane.showMessageDialog(null, "Update");
            UpdateTable();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }
    @FXML
    void obrisi_vodic(ActionEvent event) {
        connection = mySQLConnect.ConnectDB();
        String sql = "delete from Vodic where ID = ?";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,txt_ID.getText());
            preparedStatement.execute();
            JOptionPane.showMessageDialog(null, "Delete");
            UpdateTable();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    @FXML
    void Uvezi(ActionEvent event) {
        static_promjer.setText(txt_promjer.getText());
        static_presjek.setText(txt_presjek.getText());
        static_masa.setText(txt_masa.getText());
        static_modul.setText(txt_modul.getText());
        static_koeficijent.setText(txt_koef.getText());
        static_normlano.setText(txt_normalno.getText());
        static_iznimno.setText(txt_iznimno.getText());
        static_maksimalno.setText(txt_maksimalno.getText());
        ((Node)(event.getSource())).getScene().getWindow().hide();
    }
    public void UpdateTable(){
        col_ID.setCellValueFactory(new PropertyValueFactory<vodic,Integer>("ID"));
        col_promjer.setCellValueFactory(new PropertyValueFactory<vodic,String>("promjer"));
        col_presjek.setCellValueFactory(new PropertyValueFactory<vodic,String>("presjek"));
        col_masa.setCellValueFactory(new PropertyValueFactory<vodic,String>("masa"));
        col_modul.setCellValueFactory(new PropertyValueFactory<vodic,String>("elasticnost"));
        col_koeficijent.setCellValueFactory(new PropertyValueFactory<vodic,String>("koeficijentIstezanja"));
        col_normalno.setCellValueFactory(new PropertyValueFactory<vodic,String>("normlanoIstezanje"));
        col_iznimno.setCellValueFactory(new PropertyValueFactory<vodic,String>("iznimnoIstezanje"));
        col_maksimalno.setCellValueFactory(new PropertyValueFactory<vodic,String>("maksimalnoIstezanje"));

        list = mySQLConnect.getData();
        table_users.setItems(list);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        UpdateTable();
    }
}