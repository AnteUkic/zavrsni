package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.text.Text;

import javax.swing.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class ispisRezultataController{
    @FXML
    private Text provjes5;
    @FXML
    private Text provjes20;
    @FXML
    private Text provjes40;

    public Integer id;
    public Double txt_5;
    public Double txt_20;
    public Double txt_40;
    public Double promjer;
    public Double presjek;
    public Double masa;
    public Double modul;
    public Double koef;
    public Double normlano;
    public Double maksimalno;
    public Double iznimno;
    public Double h1;
    public Double h2;
    public Double raspon;
    Connection connection = null;
    ResultSet resultSet = null;
    PreparedStatement preparedStatement = null;
    public void setProvjes5(String iznos){
        provjes5.setText(iznos.substring(0,4) + " m");
        txt_5 = Double.parseDouble(iznos);
    }
    public void setProvjes20(String iznos){
        provjes20.setText(iznos.substring(0,4) + " m");
        txt_20 = Double.parseDouble(iznos);
    }
    public void setProvjes40(String iznos){
        provjes40.setText(iznos.substring(0,4) + " m");
        txt_40 = Double.parseDouble(iznos);
    }

    public void CatchData(rezultat result){
        promjer = Double.parseDouble(result.getPromjer());
        presjek = Double.parseDouble(result.getPresjek());
        masa = Double.parseDouble(result.getMasa());
        modul = Double.parseDouble(result.getElasticnost());
        koef = Double.parseDouble(result.getKoeficijentIstezanja());
        normlano = Double.parseDouble(result.getNormlanoIstezanje());
        maksimalno = Double.parseDouble(result.getMaksimalnoIstezanje());
        iznimno = Double.parseDouble(result.getIznimnoIstezanje());
        h1 = Double.parseDouble(result.getH1());
        h2 = Double.parseDouble(result.getH2());
        raspon = Double.parseDouble(result.getRaspon());
        id = result.getID();
    }
    @FXML
    void SaveToDatabase(ActionEvent event) {
        connection = mySQLConnect.ConnectDB();
        String sql = "insert into Rezultat values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id+1);
            preparedStatement.setDouble(2, promjer);
            preparedStatement.setDouble(3, presjek);
            preparedStatement.setDouble(4, masa);
            preparedStatement.setDouble(5, modul);
            preparedStatement.setDouble(6, koef);
            preparedStatement.setDouble(7, normlano);
            preparedStatement.setDouble(8, iznimno);
            preparedStatement.setDouble(9, maksimalno);
            preparedStatement.setDouble(10, h1);
            preparedStatement.setDouble(11, h2);
            preparedStatement.setDouble(12, raspon);
            preparedStatement.setDouble(13, txt_20);
            preparedStatement.setDouble(14, txt_5);
            preparedStatement.setDouble(15, txt_40);
            preparedStatement.execute();

            JOptionPane.showMessageDialog(null, "Uspjesno spremljeno");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

}
