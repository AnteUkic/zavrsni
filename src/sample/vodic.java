package sample;

public class vodic {
    Integer ID;
    String promjer;
    String presjek;
    String masa;
    String elasticnost;
    String koeficijentIstezanja;
    String normlanoIstezanje;
    String iznimnoIstezanje;
    String maksimalnoIstezanje;

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public void setPromjer(String promjer) {
        this.promjer = promjer;
    }

    public void setPresjek(String presjek) {
        this.presjek = presjek;
    }

    public void setMasa(String masa) {
        this.masa = masa;
    }

    public void setElasticnost(String elasticnost) {
        this.elasticnost = elasticnost;
    }

    public void setKoeficijentIstezanja(String koeficijentIstezanja) {
        this.koeficijentIstezanja = koeficijentIstezanja;
    }

    public void setNormlanoIstezanje(String normlanoIstezanje) {
        this.normlanoIstezanje = normlanoIstezanje;
    }

    public void setIznimnoIstezanje(String iznimnoIstezanje) {
        this.iznimnoIstezanje = iznimnoIstezanje;
    }

    public void setMaksimalnoIstezanje(String maksimalnoIstezanje) {
        this.maksimalnoIstezanje = maksimalnoIstezanje;
    }

    public Integer getID() {
        return ID;
    }

    public String getPromjer() {
        return promjer;
    }

    public String getPresjek() {
        return presjek;
    }

    public String getMasa() {
        return masa;
    }

    public String getElasticnost() {
        return elasticnost;
    }

    public String getKoeficijentIstezanja() {
        return koeficijentIstezanja;
    }

    public String getNormlanoIstezanje() {
        return normlanoIstezanje;
    }

    public String getIznimnoIstezanje() {
        return iznimnoIstezanje;
    }

    public String getMaksimalnoIstezanje() {
        return maksimalnoIstezanje;
    }

    public vodic(Integer ID, String promjer, String presjek, String masa, String elasticnost, String koeficijentIstezanja, String normlanoIstezanje, String iznimnoIstezanje, String maksimalnoIstezanje) {
        this.ID = ID;
        this.promjer = promjer;
        this.presjek = presjek;
        this.masa = masa;
        this.elasticnost = elasticnost;
        this.koeficijentIstezanja = koeficijentIstezanja;
        this.normlanoIstezanje = normlanoIstezanje;
        this.iznimnoIstezanje = iznimnoIstezanje;
        this.maksimalnoIstezanje = maksimalnoIstezanje;
    }

}
